package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public int start() {

        // TODO Write your code here
        // generating a random number
        int goal;
        double r= Math.random()*100;
        goal = (int) r;
        //System.out.println("goal ="+goal);

        //guess variable
        int guess;

        //if guess is not corect
        while (true) {
            //users guess
            System.out.print("Enter your guess: ");
            String userGuess = Keyboard.readInput();
            guess = Integer.parseInt(userGuess);
            if (guess > goal) {
                System.out.println("Too high, try again");
                //break;
            } else if (guess < goal) {
                System.out.println("Too low, try again");
                //break;
            } else {//guess == goal
                System.out.println("Perfect!");
                break;
            }
        }
        //completing game
        System.out.println("Goodbye");
        return goal;
    }


    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
